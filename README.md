# CHOSEN: A synthesis of hydrometeorological data from intensively monitored catchments
## Description
This project develops a pipeline to synthesize publicly available hydro-meteorological time series data from various resources. Using this pipeline, we have compiled data from 30 study areas from websites listed below into the **CHOSEN (Comprehensive Hydrologic Observatory SEnsor Network)** dataset (DOI: [10.5281/zenodo.4060384](https://zenodo.org/record/4060384#.YQZM8JNKhpI)). In the CHOSEN dataset, the data from different study areas have the same structures and formats, making them convenient to use for comparative hydrological studies.

Click on the study area will direct to its original website where the raw data were downloaded.

| No. | Study Area | 
| ------ | ------ | 
| 1 | [East River](http://watershed.lbl.gov/community-observatory/) | 
| 2 | [Dry Creek](https://www.boisestate.edu/drycreek/dry-creek-data/) | 
| 3 | [Sagehen Creek](https://sagehen.ucnrs.org/research/resources-data/) | 
| 4 | [Andrews Forest](https://lternet.edu/site/andrews-forest-lter/) | 
| 5 | [Baltimore](https://lternet.edu/site/baltimore-ecosystem-study/) | 
| 6 | [Bonanza Creek](https://lternet.edu/site/bonanza-creek-lter/) | 
| 7 | [California Current Ecosystem](https://lternet.edu/site/california-current-ecosystem-lter/) | 
| 8 | [Central Arizona](https://lternet.edu/site/central-arizona-phoenix-lter/) | 
| 9 | [Coweeta](https://lternet.edu/site/coweeta-lter/) | 
| 10 | [Florida Coastal Everglades](https://lternet.edu/site/florida-coastal-everglades-lter/) | 
| 11 | [Georgia Coastal Ecosystems](https://lternet.edu/site/georgia-coastal-ecosystems-lter/) | 
| 12 | [Harvard Forest](https://lternet.edu/site/harvard-forest-lter/) | 
| 13 | [Hubbard Brook](https://lternet.edu/site/hubbard-brook-lter/) | 
| 14 | [Jornada Basin](https://lternet.edu/site/jornada-basin-lter/) | 
| 15 | [Kellogg](https://lternet.edu/site/kellogg-biological-station-lter/) | 
| 16 | [Konza Prairie](https://lternet.edu/site/konza-prairie-lter/) | 
| 17 | [Northern Gulf of Alaska](https://lternet.edu/site/northern-gulf-alaska/) | 
| 18 | [Plum Island](https://lternet.edu/site/plum-island-ecosystems-lter/) | 
| 19 | [Sevilleta](https://lternet.edu/site/sevilleta-lter/) | 
| 20 | [Boulder](http://criticalzone.org/boulder/) | 
| 21 | [Catalina](http://criticalzone.org/catalina-jemez/infrastructure/field-area/santa-catalina-mountains/) | 
| 22 | [Jemez](http://criticalzone.org/catalina-jemez/infrastructure/field-area/jemez-river-basin/) | 
| 23 | [Christina](http://criticalzone.org/christina/infrastructure/field-area/white-clay-creek/) | 
| 24 | [Luquillo](http://criticalzone.org/luquillo/) |
| 25 | [Reynolds](http://criticalzone.org/reynolds/) | 
| 26 | [Shale Hills](http://criticalzone.org/shale-hills/) |
| 27 | [SanJoaquin](http://criticalzone.org/sierra/infrastructure/field-area/flux-tower-at-san-joaquin-experimental-range/#data) | 
| 28 | [Providence](http://criticalzone.org/sierra/infrastructure/field-area/providence-creek-headwaters/#data) |
| 29 | [Wolverton](http://criticalzone.org/sierra/infrastructure/field-area/wolverton-basin/) | 
| 30 | [Calhoun](http://criticalzone.org/calhoun/) |

 
From each website, we downloaded (if available) field measured time series data of streamflow, precipitation, air temperature, solar radiation, evapotranspiration, relative humidity, wind direction, wind speed, SWE, snow depth, snowmelt, vapor pressure, soil moisture, soil temperature, and water isotopes.

There are two folders under the directory. The DataPipeline folder includes all the Jupyter Notebooks used to process the data in each study area. The DataProduct folder contains the clean-up version of data in NetCDF format, along with a Jupyter Notebook (0_Extract_data_from_nc.ipynb) for extracting data and information from NetCDF. 

## Data

For the convenience of data users, we provided the data in two formats: .nc (NetCDF) and .csv. 
Check this [link](https://pro.arcgis.com/en/pro-app/latest/help/data/multidimensional/what-is-netcdf-data.htm#:~:text=NetCDF%20) for an introduction about NetCDF file format.

To extract data from NetCDF files, go to the **DataProduct** folder and download the **Jupyter Notebook (0_Extract_Data_From_NetCDF.ipynb)** and data files (.nc). The Jupyter Notebook is a tutorial about extracting data and information from NetCDF files. Geographical Information about monitoring stations is also available to obtain using the Notebook. 

To check data from .csv files, go to the **DataPipeline** folder, click the folder for a study area. Under the **DailyData** folder, the file “5_StudyArea_final_cleaned.csv” is the final cleaned version of daily data. There are multiple other data files which were generated when the raw data was processed through the pipeline.

## Metadata

The metadata provided include time range of record, variable unit and name, and geographical information of hydro-meteorologcial stations.

Those information can be extracted from the NetCDF files using the **Jupyter Notebook (0_Extract_Data_From_NetCDF.ipynb)**.

## Acknowledgements
This work is supported by the US Geological Survey Powell Center for Analysis and Synthesis, a Gordon and Betty Moore Foundation Data-Driven Discovery Investigator grant to LL, and the Jupyter Meets the Earth project, funded by NSF grant number (UC Berkeley: 1928406, NCAR: 1928374). Partial support for ASW is provided by the National Science Foundation and the Experimental Program to Stimulate Competitive Research (EPSCoR: EPS-1929148; Canary in the Watershed). Much of the data used in this study were available from the US. Long-Term Ecological Research Network, Critical Zone Observatory program, Lawrence Berkeley National Laboratory, Dry Creek Experimental Watershed (DCEW); we would like to acknowledge all the staff from these institutions for collecting and publicizing the data. We thank Dr. Adrian Harpold for providing the data from the Sagehen catchment. We would especially like to thank the Powell Center Working Group on Watershed Storage and Controls for their contributions to this project. We also thank Dr. Lindsey Heagy and Dr. Fernando Pérez for their suggestions on data publication and future development.

## Collaboration

Please contact Berkeley ESDL lab or email angelikazhang@berkeley.edu if you have any questions about the CHOSEN dataset or would like to contribute data from another study area.

